Q(t)Imgur Simple Client
=======================

#Introduction

This is quiet small and simple script to upload, get image, album info, and so
on... from [Imgur](imgur.com).

#Usage
Run this script from command line. Use the `-h` option to get help.

#TODO
- Complete and inprove current features.
- Persistent data store.
- Authentication.
- Friendly GUI.
- ...
