#!/usr/bin/env python
# -*- coding: utf-8 -*-

#=============================================================================#
# File Name: authen.py
# Creation Date: 16:21:00 19-05-2012 ICT
# Last Modified: Sun May 20, 15:21:31 ICT 2012
# Created by: xuansamdinh
# Email: xuansamdinh.n2i@gmail.com
# Description:
#=============================================================================#

import pycurl
import time
import oauth.oauth as oauth


# URLs
REQUEST_TOKEN_URL = 'https://api.imgur.com/oauth/request_token'
AUTHORIZATION_URL = 'https://api.imgur.com/oauth/authorize'
ACCESS_TOKEN_URL  = 'https://api.imgur.com/oauth/access_token'
# key and secret granted by the service provider for this consumer application
class OauthImgur(oauth.OAuthClient):
    def __init__(self, request_token_url='',
            access_token_url='',  authorization_url=''):
        self.request_token_url = request_token_url
        self.access_token_url = access_token_url
        self.authorization_url = authorization_url
        self

# vim: set ff=unix ft=python fenc=utf-8 ts=4 sw=4 tw=79 :
