#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "xuansamdinh"
__copyright__ = "Copyright (c) 2012 xuansamdinh"
__credits__ = ["xuansamdinh"]
__email__ = "xuansamdinh.n2i@gmail.com"

__version__ = "0.0.1"
__liciense__ = "GPL"
__maintainer__ = ["xuansamdinh"]
__status__ = "Development"

import sys
import os
import cStringIO
import optparse
import tempfile


from xml.etree import cElementTree as ET
#try:
#    from xml.etree import cElementTree as ET
#except:
#    print "The cElementTree module doesn't exist, use the ElementTree\
#            instead now"
#    from xml.etree import ElementTree as ET

try:
    import pycurl
except:
    print "You need to have pycurl installed. You can get it from: \
            http://pycurl.sourceforge.net/"
    sys.exit(2)

UPLOAD_URL = "http://api.imgur.com/2/upload"
IMAGE_URL = "http://api.imgur.com/2/image/"
ALBUM_URL = "http://api.imgur.com/2/album/"
DELETE_URL = "http://api.imgur.com/2/delete/"
STATS_URL = "http://api.imgur.com/2/stats"
KEY = "c38ab9585c6cc0a837140c2fdb435a2c"
TEMP_DIR = tempfile.gettempdir()
TEMP_FILE = "qimgur.tmp"


class AnonyImgur():
    """Using Imgur Anonymous API.

    Upload: Get image, album info; And so on...
    """

    def __init__(self, options, img=None):
        self.upload_url = UPLOAD_URL
        self.key = KEY
        self.img = img
        self.data = ""
        self.imginfo = {}
        self.links = {}
        self.options = options

    def upload(self):
        up = pycurl.Curl()
        buf = cStringIO.StringIO()
        key = ("key", self.key)
        image = ("image", (up.FORM_FILE, self.img))
        values = [key, image]
        up.setopt(up.URL, self.upload_url)
        up.setopt(up.HTTPPOST, values)
        if self.options.verbose:
            up.setopt(up.VERBOSE, True)
        up.setopt(up.WRITEFUNCTION, buf.write)
        up.perform()
        up.close()
        return buf.getvalue()

    def imageinfo(self):
        up = pycurl.Curl()
        buf = cStringIO.StringIO()
        image_url = (IMAGE_URL + "%s.xml") % self.options.ihash
        up.setopt(up.URL, image_url)
        up.setopt(up.WRITEFUNCTION, buf.write)
        if self.options.verbose:
            up.setopt(up.VERBOSE, True)
        up.perform()
        up.close()
        return buf.getvalue()

    def albuminfo(self):
        up = pycurl.Curl()
        buf = cStringIO.StringIO()
        album_url = (ALBUM_URL + "%s.xml") % self.options.albumid
        up.setopt(up.URL, album_url)
        up.setopt(up.WRITEFUNCTION, buf.write)
        if self.options.verbose:
            up.setopt(up.VERBOSE, True)
        up.perform()
        up.close()
        return buf.getvalue()

    def delete_img(self):
        up = pycurl.Curl()
        del_image = DELETE_URL + self.options.dhash
        up.setopt(up.URL, del_image)
        if self.options.verbose:
            up.setopt(up.VERBOSE, True)
        up.perform()
        up.close()
        print "It maybe ok :3"

    def write_hash(delete_hash):
        try:
            open(os.path.join(TEMP_DIR, TEMP_DILE), 'w').write(delete_hash)
        except IOError:
            print "Ooop"

    def getinfo(self, dataxml):
        data = ET.fromstring(dataxml)
        imginfo = {}
        links = {}

        for value in data.findall("image/"):
            imginfo[value.tag] = value.text
        for value in data.findall("links/"):
            links[value.tag] = value.text

        return imginfo, links

    def print_info(self, imginfo, links):
        for value in imginfo.keys():
            print "%15s: %s" % (value, imginfo[value])
        for value in links.keys():
            print "%15s: %s" % (value, links[value])

    def run(self):
        if self.options.ihash is not None:
            print "Getting info of %s" % self.options.ihash
            data = self.imageinfo()
            self.imginfo, self.links = self.getinfo(data)
            self.print_info(self.imginfo, self.links)
        elif self.options.dhash is not None:
            self.delete_img()
        elif self.options.albumid is not None:
            print "Album ID is %s" % self.options.albumid
            albuminfo = self.albuminfo()
            print albuminfo
        else:
            data = self.upload()
            if self.options.output is not None:
                try:
                    f_output = open(self.options.output, 'w')
                    f_output.write(data)
                    f_output.close()
                    print "Write to %s done" % self.options.output
                except IOError:
                    print "Please check w/r permission"
            else:
                self.imginfo, self.links = self.getinfo(data)
                self.print_info(self.imginfo, self.links)


def main():
    parser = optparse.OptionParser(usage="usage: %prog [option] [filename]",
                                    version="%prog 0.0.1")
    parser.add_option("-i", "--ihash",
                        action="store",
                        dest="ihash",
                        help="Image hash to get info")
    parser.add_option("-a", "--albumid",
                        action="store",
                        dest="albumid",
                       help="Album ID to get info")
    parser.add_option("-o", "--output",
                        action="store",
                        dest="output",
                        help="Output file to store the response")
    parser.add_option("-d", "--dhash",
                        action="store",
                        dest="dhash",
                        help="Delete hash")
    parser.add_option("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        help="Verbose")
    (options, args) = parser.parse_args()
    if options.ihash is not None or options.albumid is not None:
        new = AnonyImgur(options)
        new.run()
    elif len(args) != 1: # or len(args) == 0:
#        new = QImgur(options)
#        new.run()
        parser.error("Please specify one valid file name or path")
    else:
        filename = args[0]
        new = AnonyImgur(options, img=filename)
        new.run()


if __name__ == "__main__":
    main()
    sys.exit()
