#!/usr/bin/env python
# -*- coding: utf-8 -*-

#=============================================================================#
# File Name: qimgur.py
# Creation Date: 13:58:54 28-05-2012 ICT
# Last Modified: Tue May 29, 02:42:35 ICT 2012
# Created by: xuansamdinh
# Email: xuansamdinh.n2i@gmail.com
# Description:
#=============================================================================#

__author__ = "xuansamdinh"
__copyright__ = "Copyright (c) 2012 xuansamdinh"
__credits__ = ["xuansamdinh"]
__email__ = "xuansamdinh.n2i@gmail.com"

__version__ = "0.0.1"
__liciense__ = "GPL"
__maintainer__ = ["xuansamdinh"]


import sys
import os
import imghdr
import cStringIO
#import optparse
#import tempfile


from xml.etree import cElementTree as ET

try:
    import pycurl
except:
    print "You need to have pycurl installed. You can get it from: \
            http://pycurl.sourceforge.net/"
    sys.exit(2)

from PyQt4 import QtGui, QtCore
from qimgurUi import Ui_MainWindow

from AnonyImgur import Anonymous

#UPLOAD_URL = "http://api.imgur.com/2/upload"
#IMAGE_URL = "http://api.imgur.com/2/image/"
#ALBUM_URL = "http://api.imgur.com/2/album/"
#DELETE_URL = "http://api.imgur.com/2/delete/"
#STATS_URL = "http://api.imgur.com/2/stats"
KEY = "c38ab9585c6cc0a837140c2fdb435a2c"
#TEMP_DIR = tempfile.gettempdir()
#TEMP_FILE = "qimgur.tmp"

dbdir = os.path.join(os.path.expanduser("~"),".qimgur")
#dbfile = os.path.join(dbdir, "qimgur.sqlite")

class QImgur(QtGui.QMainWindow):
    """Simple GUI application."""
    def __init__(self):
        super(QImgur,self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.validTypes = ['jpg', 'png', 'jpeg', 'gif']
        self.key = KEY
        self.filename = None
        self.upimginfo = None

        self.ui.pushSelectFile.clicked.connect(self.selectFile)
        self.ui.pushUpload.clicked.connect(self.uploadanony)
        self.ui.pushReset.clicked.connect(self.resetFilename)
        self.ui.pushQtAbout.clicked.connect(self.qtabout)

        self.ui.lineimghash.returnPressed.connect(self.fetchInfo)
        self.ui.pushFetchinfo.clicked.connect(self.fetchInfo)

        QtGui.QShortcut(QtGui.QKeySequence("Ctrl+Q"), self, self.close)

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()


    def selectFile(self):
        try:
            self.filename = str(QtGui.QFileDialog.getOpenFileName().toAscii())
            filetype = imghdr.what("%s" % self.filename)
            if not filetype:
                errmsg = "Err: Y U NO select image file but another type!"
                typeerr = "Please select an image file, not another!"
                self.notifyDialog(errmsg, typeerr)
                self.filename = None
            else:
                if filetype not in self.validTypes:
                    errmsg = "Err: Y U NO select a valid image file type!"
                    typeerr = "Valid types: jpg, jpeg, png, gif. Please choose"
                    "corrent file type!"
                    self.notifyDialog(errmsg, typeerr)
                    self.filename = None
                else:
                    self.ui.lineSelectFile.setText("Selected file: %s" % self.filename)

            self.ui.statusbar.showMessage("You have selected: %s" %
                    self.filename, msecs= 5)
        except IOError:
            errmsg = "Err: Some error has been occured or Y U NO select file!"
            errmore = "Dunu exactly, please recheck by yourself"
            self.notifyDialog(errmsg, errmore)
            self.filename = None

    def notifyDialog(self, errmsg, detailtext=None):
        notifyDialog = QtGui.QMessageBox(self, windowTitle="Notification Dialog",
                    text="%s" % errmsg)
        if detailtext is not None:
            notifyDialog.setDetailedText(detailtext)
        notifyDialog.exec_()

    def printFilename(self):
        if self.filename is None:
            errmsg = "Err: Please select a image file to upload!"
            notifyDialog = QtGui.QErrorMessage(self)
            notifyDialog.showMessage(errmsg)
        else:
            msgDialog = QtGui.QMessageBox(self, windowTitle="Done", text="%s" %
                    self.filename)
            msgupload = "Your file has been upload successfully"
            msgDialog.setDetailedText(msgupload)
            msgDialog.exec_()

    def uploadanony(self):
        anonyup = Anonymous(self.filename, self.key)
        try:
            self.upimginfo = anonyup.upload()
#            f_upimginfo = open("output.xml", 'w')
#            f_upimginfo.write(self.upimginfo)
#            f_upimginfo.close()
#            try:
            self.showProperties(self.ui.list, self.upimginfo)
#            except:
#                print "Something was wrong!"
            upnoti = "Uploading OK"
            self.notifyDialog(upnoti)
        except:
            upnoti = "Uploading failed! Some Error has been occured!"
            self.notifyDialog(upnoti)

    def fetchInfo(self):
        ihash = str(self.ui.lineimghash.text())
        if len(ihash) is not 5:
            hashnoti = "Please specify a valid hash number!"
            hashdetail = "Hash number contain five chars and only file chars!"
            self.notifyDialog(hashnoti, hashdetail)
        else:
            image_url = "http://api.imgur.com/2/image/%s" % ihash
            try:
                up = pycurl.Curl()
                buf = cStringIO.StringIO()
                up.setopt(up.URL, image_url)
                #up.setopt(up.VERBOSE, True)
#                up.setopt(up.CONNECTIONTIMEOUT, 150)
                up.setopt(up.TIMEOUT, 30)
                up.setopt(up.WRITEFUNCTION, buf.write)
                up.perform()
                up.close()
                data = buf.getvalue()
                self.showProperties(self.ui.list_2, data)
            except:
                errfetch = "Some error has occured when trying to fetch info"
                self.notifyDialog(errfetch)

    def showProperties(self, treelist, data):
        data = ET.fromstring(data)
        if not os.path.isdir(dbdir):
            os.mkdir(dbdir)
        outputfile = os.path.join(dbdir, data.find('.//hash').text + ".xml")
        if not os.path.exists(outputfile):
            f_output = open(outputfile, 'w')
            f_output.write(ET.tostring(data))
            f_output.close()
        treelist.clear()
        for element in data.findall('image/'):
            propertie, value = element.tag, element.text
            if value is None:
                value = "<No specify>"
            item = QtGui.QTreeWidgetItem([propertie, value])
            treelist.addTopLevelItem(item)

        for element in data.findall('links/'):
            propertie, value = element.tag, element.text
            if value is None:
                value = "<No specify>"
            item = QtGui.QTreeWidgetItem([propertie, value])
            treelist.addTopLevelItem(item)


    def resetFilename(self):
        self.filename = None
        self.ui.statusbar.showMessage("You have discard select image!", msecs=5)

    def qtabout(self):
        QtGui.QMessageBox.aboutQt(self, "Qt")



def main():
    app = QtGui.QApplication(sys.argv)
    qimgurwin = QImgur()
    qimgurwin.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
