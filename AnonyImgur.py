#!/usr/bin/env python
# -*- coding: utf-8 -*-

#=============================================================================#
# File Name: AnonyImgur.py
# Creation Date: 21:48:08 28-05-2012 ICT
# Last Modified: Tue May 29, 01:10:34 ICT 2012
# Created by: xuansamdinh
# Email: xuansamdinh.n2i@gmail.com
# Description:
#=============================================================================#

__author__ = "xuansamdinh"
__copyright__ = "Copyright (c) 2012 xuansamdinh"
__credits__ = ["xuansamdinh"]
__email__ = "xuansamdinh.n2i@gmail.com"

__version__ = "0.0.1"
__liciense__ = "GPL"
__maintainer__ = ["xuansamdinh"]
__status__ = "Development"

import sys
import os
import cStringIO
#import optparse
#import tempfile


from xml.etree import cElementTree as ET

try:
    import pycurl
except:
    print "You need to have pycurl installed. You can get it from: \
            http://pycurl.sourceforge.net/"
    sys.exit(2)


class Anonymous():
    """Use Imgur Anonymous API.

    Upload, get statics, get image info,...
    """

    def __init__(self, img=None, key=None, ihash=None):
        self.img = img
        self.key = key
        self.ihash = ihash

    def upload(self):
        up = pycurl.Curl()
        buf = cStringIO.StringIO()
        key = ("key", self.key)
        image = ("image", (up.FORM_FILE, self.img))
        paras = [key, image]
        up.setopt(up.URL, "http://api.imgur.com/2/upload")
        up.setopt(up.HTTPPOST, paras)
        up.setopt(up.VERBOSE, True)
        up.setopt(up.WRITEFUNCTION, buf.write)
        up.perform()
        up.close()
        return buf.getvalue()

    def fetchInfo(self):
        print "one"
        image_url = "http://api.imgur.com/2/image/%s" % self.ihash
        print "two"
        up = pycurl.Curl()
        buf = cStringIO.StringIO()
        up.setopt(up.URL, image_url)
        up.setopt(up.VERBOSE, True)
        up.setopt(up.WRITEFUNCTION, buf.write)
        up.perform()
        print "there"
        up.close()
        return buf.getvalue()
